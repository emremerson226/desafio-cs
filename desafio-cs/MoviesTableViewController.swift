//
//  MoviesTableViewController.swift
//  desafio-cs
//
//  Created by e.de.farias.batista on 19/09/21.
//

import Foundation
import UIKit

class MoviesTableViewController: UITableViewController {
    
    var name: String = ""
    var movies: [Movie] = []
    
    
    var loadingMovies = false
    var currentPage = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        loadMovies()
    }

    func loadMovies() {
        loadingMovies = true
        MovieAPI.loadMovies(name: name, page: currentPage) { info in
            if let info = info {
                self.movies += info.results
                
                
                DispatchQueue.main.async {
                    self.loadingMovies = false
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieTableViewCell", for: indexPath)
        
        let movie = movies[indexPath.row]
        cell.textLabel?.text = movie.title
        return cell
    }
}


