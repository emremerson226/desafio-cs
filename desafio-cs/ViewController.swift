//
//  ViewController.swift
//  desafio-cs
//
//  Created by e.de.farias.batista on 06/09/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tfName: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        tfName.resignFirstResponder()
        let vc = segue.destination as! MoviesTableViewController
        vc.name = tfName.text ?? ""
    }
}
