//
//  MovieAPI.swift
//  desafio-cs
//
//  Created by e.de.farias.batista on 19/09/21.
//

import Foundation
import Alamofire


class MovieAPI {
    static private let basePath   = "https://api.themoviedb.org/3/search/movie?"
    static private let privateKey = "ea0bb72f2844501d314cac56ed2b5d10"
    static private let limit      = 30
  
    class func loadMovies(name: String, page:Int = 0, onComplete: @escaping (DiscoverMovieResponse?) -> Void) {
        let offset = page * limit
        let url = basePath + "offset=\(offset)&limit=\(limit)&api_key=\(privateKey)&query=\(name)"
        print(url)
        
        AF.request(url).responseJSON { response in
            guard let data = response.data,
                  let discoverMovieResponse = try? JSONDecoder().decode(DiscoverMovieResponse.self, from: data) else {
                      onComplete(nil)
                      return
                  }
            onComplete(discoverMovieResponse)
        }
    }
}
